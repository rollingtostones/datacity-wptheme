<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */
$the_theme = wp_get_theme();
$container = get_theme_mod( 'understrap_container_type' );
?>
<?php get_sidebar( 'footerfull' ); ?>

<section id="footer" class="bg-lead text-white d-print-none">
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-sm-4">
          <a href="<?php echo get_home_url() ?>">
            <img src="<?php echo get_template_directory_uri(); ?>/img/logo-datacity-small.png" 
              alt="Datacity IoT" 
              class="img-fluid">
          </a>
        </div>
        <div class="col-sm mt-4 mt-sm-0 pt-4 pt-sm-0">
          <div id="colaboradores" class="pb-4">
            <b>COLABORADORES</b>
            <div class="d-sm-flex mt-2 align-items-end">
              <div class="mr-0 mr-sm-4 pr-0 pr-sm-3 mb-4 mb-sm-0">
                <img src="<?php echo get_template_directory_uri(); ?>/img/logo-libelium.png" alt="Libelium">
              </div>
              <div class="mr-0 mr-sm-4 pr-0 pr-sm-3 mb-4 mb-sm-0">
                <img src="<?php echo get_template_directory_uri(); ?>/img/logo-corfo.png" alt="CORFO">
              </div>
              <div class="mr-0 mr-sm-4 pr-0 pr-sm-3 mb-4 mb-sm-0">
                <img src="<?php echo get_template_directory_uri(); ?>/img/logo-prochile.png" alt="PRO|CHILE">
              </div>
            </div>
          </div>
          <div class="d-sm-flex mt-4 pb-4">
            <div id="siguenos" class="mr-0 mr-sm-4 pr-0 pr-sm-3">
              <b>SIGUENOS POR</b>
              <div class="container-fluid siguenos">
                <div class="row">
                  <div class="pr-2 pt-2">
                    <a target="_blank"  href="https://www.facebook.com/DatacityIoT/">
                      <img src="<?php echo get_template_directory_uri(); ?>/img/icono-facebook.png" alt="facebook">
                    </a>
                  </div>
                  <div class="pr-2 pt-2">
                    <a target="_blank" href="https://twitter.com/DatacityIoT">
                      <img src="<?php echo get_template_directory_uri(); ?>/img/icono-twitter.png" alt="twitter">
                    </a>
                  </div>
                  <div class="pr-2 pt-2">
                    <a target="_blank" href="https://www.linkedin.com/company/datacity-iot/">
                      <img src="<?php echo get_template_directory_uri(); ?>/img/icono-linkedin.png" alt="linkedin">
                    </a>
                  </div>
                  <div class="pr-2 pt-2">
                    <a target="_blank" href="https://www.instagram.com/datacityiot/">
                      <img src="<?php echo get_template_directory_uri(); ?>/img/icono-instagram.png" alt="instagram">
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div id="contactanos" class="pt-4 pt-sm-0">
              <b>CONT&Aacute;CTANOS</b>
              <div class="mt-2">
                <div id="direccion" class="mb-2 d-flex">
                  <div class="pt-2 pr-3">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/marker.png" 
                      class="navbar-icon" 
                      alt="Direccion">
                  </div>
                  <div>
                    <div class="d-block d-md-inline">
                      AV. ECHEÑIQUE 5839,
                    </div>
                    <div class="d-block d-md-inline">
                      OF 512, LA REINA,
                    </div>
                    <div class=" d-block">
                      SANTIAGO - CHILE
                    </div>
                  </div>
                </div>
                <div id="telefono" class="d-flex">
                  <div class="pt-2 pt-md-0 pr-2">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/icono-phone.png" 
                      class="navbar-icon" 
                      alt="Telefono">
                  </div>
                  <div>
                    <div class="mr-2 mr-md-0 d-block d-md-inline">
                      +562 29935452
                    </div>
                    <div class=" d-block d-md-inline">
                      +562 29935453
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div id="copyright" class="mt-4">
            <span>
              COPYRIGHT &copy; 2018 | 
              RESERVADOS TODOS LOS DERECHOS | 
              DATACITY IoT S.p.A.
            </span>
          </div>
        </div>
      </div>
    </div>
    <div class="totop">
      <a href="#top">
        <div class="totop-button ml-auto">
          <img src="<?php echo get_template_directory_uri(); ?>/img/icono-totop.png" alt="Subir">
        </div>
      </a>
    </div>
  </footer>
</section>

</div><!-- #page we need this extra closing tag here -->
<?php wp_footer(); ?>
</body>
<script 
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDSTbWuT4ei-93LounNK3nLNi3ZYly4zNU&callback=initMap" 
  async defer>
</script>
<script>
</script>
</html>

