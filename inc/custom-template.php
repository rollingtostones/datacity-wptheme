<?php

function custom_posts_templates( $single_template ){
    if(!is_singular()){
        return;
    }

    if( in_category( 'soluciones' ) ){
        $single_template = locate_template( 'page-templates/soluciones.php' );
        return $single_template;
    }
    if( in_category( 'proyectos' ) ){
      $single_template = locate_template( 'page-templates/proyectos.php' );
      return $single_template;
    }
    if( in_category( 'participamos' ) ){
      $single_template = locate_template( 'page-templates/participamos.php' );
      return $single_template;
    }
    
}
add_filter( 'single_template', 'custom_posts_templates' );