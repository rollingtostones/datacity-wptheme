<?php
get_header();
$container   = get_theme_mod( 'understrap_container_type' );
?>
<section id="top">
    <?php get_template_part( 'global-templates/topnav' ); ?>
</section>

<section id="contacto">
  <div class="header bg-black">
    <h1>Contacto</h1>
    <span>
      - SOLICITA NUESTRAS SOLUCIONES -
    </span>
  </div>
  <div class="map-world"></div>
  <div id="map" class="bg-lightgray"></div>
  <div class="container map-footer my-5">
    <h4 class="text-lightblue kelsonBold px-2">
      - CHILE -
    </h4>
    <hr>
    <div class="d-flex pl-4">
      <div class="mr-2">
        <img class="mt-2" src="img/marker.png">
      </div>
      <div class="w-100">
        <b class="kelsonBold align-top">
          SANTIAGO
        </b>
        <p class="m-0">
          PJE. REP&Uacute;BLICA 54, SANTIAGO, 
          REGI&Oacute;N METROPOLITANA
        </p>
        <div class="d-md-flex justify-content-between">
          <div class="whatsapp kelsonBold">
            <img src="img/icono-whatsapp.png">
            +569 44923414
          </div>
          <div class="tag mt-2 mt-md-0">
            INFO@DATACITYIOT.COM
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>